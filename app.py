from flask import Flask, request,jsonify, send_file
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import datetime
import os


class MyFlaskApp(Flask):
    def run(self, host=None, port=None, debug=None, load_dotenv=True, **options):
        if 1:
            with self.app_context():
                pass
        super(MyFlaskApp, self).run(host=host, port=port, debug=debug, load_dotenv=load_dotenv, **options)


app = MyFlaskApp(__name__)
app.config['SQLALCHEMY_DATABASE_URI']="postgresql://roman:testkey@127.0.0.1:7000/shk"
CORS(app)
db = SQLAlchemy(app)
MEDIA_FOLDER="media"

class News(db.Model):
    __tablename__="news"
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Text)
    content = db.Column(db.Text)
    image_url = db.Column(db.Text)

@app.route("/news/",methods=['GET'])
def get_news():
    resp = []
    for news in News.query.all():
        tmp = {}
        tmp['title']=news.title
        tmp['date']=news.date
        tmp['short']=news.content[0:250]+"..."
        tmp['url'] = news.image_url
        tmp['id']= news.id
        resp.append(tmp)
    return resp


@app.route('/news/<id_arg>/',methods=['GET'])
def get_news_by_id(id_arg):
    news = None
    for n in News.query.all():
        if n.id == int(id_arg):
            news = n
            break
    if not news:
        return jsonify(message='not found'),404
    return jsonify(message='success',title=news.title,content = news.content,date=news.date,img_url=news.image_url,id=news.id)


@app.route('/news/<id>/',methods=['DELETE'])
def delete_news(id):
    return f"news by {id} was deleted"

@app.route('/news/',methods=['POST'])
def create_news():
    title = request.json['title']
    content = request.json['content']
    try:
        img_url = request.json['img_url']
    except:
        img_url = "none"
    try:
        all_news = News().query.all()
        news = News(title=title,content=content,date=datetime.datetime.now(), image_url=img_url,id=len(all_news)+1)
        db.session.add(news)
        db.session.commit()
        return jsonify(message="success"),200
    except Exception as e:
        print(e)
        return jsonify(message='fail'),500


@app.route('/news/<id>/',methods=['PUT'])
def update_news(id):
    return f"news by {id} was updated"



@app.route('/photos/',methods=['GET'])
def get_photos():
    resp = []
    for file in os.listdir(MEDIA_FOLDER+"/"):
        resp.append(file)
    return resp

@app.route('/photos/<id>/',methods=['DELETE'])
def delete_photo(id):
    return f"photo by {id} was deleted"

@app.route('/photos/<path>/',methods=['GET'])
def get_photo(path):
    print(path)
    if os.path.exists(MEDIA_FOLDER+"/"+path)==False:
        return jsonify(message="not found"),404
    return send_file(MEDIA_FOLDER+"/"+path)

@app.route('/photos/<id>/',methods=['POST'])
def create_photo(id):
    return f"photo by {id} was created"

@app.route('/photos/<id>/',methods=['PUT'])
def update_photo(id):
    return f"photo by {id} was updated"
